<?php

use App\Models\StatusAssigment;
use App\Models\StatusPay;
use App\Models\TypeAssigment;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->get('/chartsjs', 'ChartsjsController@index')->name('chartsjs');

    $router->resource('brand', BrandController::class);
    $router->resource('AssistanceVehicle', AssistanceVehicleController::class);
    $router->resource('TypeAssistanceVehicle', TypeAssistanceVehicleController::class);
    $router->resource('vehicle', VehicleController::class);
    $router->resource('assigment_active', AssigmentController::class);
    $router->resource('type_bills', TypeBillsController::class);
    $router->resource('type_assigment', TypeAssigmentController::class);
    $router->resource('status_pay', StatusPayController::class);
    $router->resource('status_assigment', StatusAssigmentController::class);
    $router->resource('customer', CustomerController::class);


    /*$router->resource('chartsjs', ChartsjsController::class);*/
});
