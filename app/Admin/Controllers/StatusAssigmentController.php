<?php

namespace App\Admin\Controllers;

use App\Models\StatusAssigment;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Database\Eloquent\Model;

class StatusAssigmentController extends AdminController
{
    //
    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\StatusAssigment';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid(new StatusAssigment());
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('description', __('description'));
        $grid->model()->orderBy('id', 'desc');

        return $grid;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {
        $show = new Show(StatusAssigment::findOrFail($id));
        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('description', __('description'));

        return $show;
    }

    /**
     * Make a form builder.
     * @return Form
     */
    protected function form () {
        $form = new Form(new StatusAssigment());
        $form->text('name', __('Name'));
        $form->text('description', __('description'));
        return $form;
    }
}
