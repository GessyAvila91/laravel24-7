<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\AssistanceVehicle;
use Illuminate\Database\Eloquent\Model;

class AssistanceVehicleController extends AdminController {
    //
    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\Catalog\AssistanceVehicle';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid(new AssistanceVehicle());

        $grid->column('id', __('Id'));
        $grid->column('plate', __('Plate'));
        $grid->column('alias', __('Alias'));
        $grid->column('user_code', __('User/Driver'));
        $grid->column('brand', __('Brand'));
        $grid->column('model', __('Model'));
        $grid->column('version', __('Version'));
        $grid->column('photo', __('Photo'));
        $grid->column('created_at', __('created_at'));
        $grid->column('updated_at', __('updated_at'));
        $grid->column('release_at', __('release_at'));

        $grid->model()->orderBy('id', 'desc');

        return $grid;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {
        $show = new Show(AssistanceVehicle::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('plate', __('plate'));
        $show->field('alias', __('alias'));
        $show->field('user_code', __('user_code'));
        $show->field('brand', __('brand'));
        $show->field('model', __('model'));
        $show->field('version', __('version'));
        $show->field('photo', __('photo'));
        $show->field('created_at', __('created_at'));
        $show->field('updated_at', __('updated_at'));
        $show->field('release_at', __('release_at'));


        return $show;
    }

    /**
     * Make a form builder.
     * @return Form
     */
    protected function form () {
        $form = new Form(new AssistanceVehicle());

        $form->text('plate', __('plate'));
        $form->text('alias', __('alias'));
        $form->text('user_code', __('user_code'));
        $form->text('brand', __('brand'));
        $form->text('model', __('model'));
        $form->text('version', __('version'));
        $form->text('photo', __('photo'));
        $form->text('created_at', __('created_at'));
        $form->text('updated_at', __('updated_at'));
        $form->text('release_at', __('release_at'));

        return $form;
    }
}
