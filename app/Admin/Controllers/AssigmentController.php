<?php

namespace App\Admin\Controllers;

use App\Models\Assigment;
use App\Models\AssistanceVehicle;
use App\Models\Customer;
use App\Models\TypeAssigment;
use App\Models\Vehicle;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class AssigmentController extends AdminController {
    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\Assigments\Active';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid(new Assigment());

        $grid->column('id', __('id'));
        $grid->column('status_pay_id', __('status_pay_id'));
        $grid->column('status_assigment_id', __('status_assigment_id'));
        $grid->column('customer_id', __('customer_id'));
        $grid->column('type_assigment_id', __('type_assigment_id'));
        $grid->column('vehicle_id', __('vehicle_id'));
        $grid->column('version', __('version'));
        $grid->column('type_bulls_id', __('type_bulls_id'));
        $grid->column('assistance_vehicle_id', __('assistance_vehicle_id'));
        $grid->column('color', __('color'));
        $grid->column('plate', __('plate'));
        $grid->column('date_capture', __('date_capture'));
        $grid->column('date_proses', __('date_proses'));
        $grid->column('date_contact', __('date_contact'));
        $grid->column('date_over', __('date_over'));
        $grid->column('date_pay', __('date_pay'));
        $grid->column('date_lastupdate', __('date_lastupdate'));
        $grid->column('report', __('report'));
        $grid->column('sinester', __('sinester'));
        $grid->column('location', __('location'));
        $grid->column('destination', __('destination'));
        $grid->column('cost_lead_change_name', __('cost_lead_change_name'));
        $grid->column('distance', __('distance'));
        $grid->column('cost_distance', __('cost_distance'));
        $grid->column('guard_day', __('guard_day'));
        $grid->column('cost_guard', __('cost_guard'));
        $grid->column('cost_expired', __('cost_expired'));
        $grid->column('extra_load', __('extra_load'));
        $grid->column('cost_load', __('cost_load'));
        $grid->column('handling', __('handling'));
        $grid->column('cost_handling', __('cost_handling'));
        $grid->column('armor', __('armor'));
        $grid->column('dolly', __('dolly'));
        $grid->column('amount', __('amount'));
        $grid->column('amountAux', __('amountAux'));
        $grid->column('bill', __('bill'));
        $grid->column('billFilePDF', __('billFilePDF'));
        $grid->column('billFileXml', __('billFileXml'));
        $grid->column('tax', __('tax'));
        $grid->column('coment', __('coment'));


        return $grid;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {
        $show = new Show(Assigment::findOrFail($id));

        $show->field('id', __('Id'));

        $show->field('id', __('id'));
        $show->field('status_pay_id', __('status_pay_id'));
        $show->field('status_assigment_id', __('status_assigment_id'));
        $show->field('customer_id', __('customer_id'));
        $show->field('type_assigment_id', __('type_assigment_id'));
        $show->field('vehicle_id', __('vehicle_id'));
        $show->field('version', __('version'));
        $show->field('type_bulls_id', __('type_bulls_id'));
        $show->field('assistance_vehicle_id', __('assistance_vehicle_id'));
        $show->field('color', __('color'));
        $show->field('plate', __('plate'));
        $show->field('date_capture', __('date_capture'));
        $show->field('date_proses', __('date_proses'));
        $show->field('date_contact', __('date_contact'));
        $show->field('date_over', __('date_over'));
        $show->field('date_pay', __('date_pay'));
        $show->field('date_lastupdate', __('date_lastupdate'));
        $show->field('report', __('report'));
        $show->field('sinester', __('sinester'));
        $show->field('location', __('location'));
        $show->field('destination', __('destination'));
        $show->field('cost_lead_change_name', __('cost_lead_change_name'));
        $show->field('distance', __('distance'));
        $show->field('cost_distance', __('cost_distance'));
        $show->field('guard_day', __('guard_day'));
        $show->field('cost_guard', __('cost_guard'));
        $show->field('cost_expired', __('cost_expired'));
        $show->field('extra_load', __('extra_load'));
        $show->field('cost_load', __('cost_load'));
        $show->field('handling', __('handling'));
        $show->field('cost_handling', __('cost_handling'));
        $show->field('armor', __('armor'));
        $show->field('dolly', __('dolly'));
        $show->field('amount', __('amount'));
        $show->field('amountAux', __('amountAux'));
        $show->field('bill', __('bill'));
        $show->field('billFilePDF', __('billFilePDF'));
        $show->field('billFileXml', __('billFileXml'));
        $show->field('tax', __('tax'));
        $show->field('coment', __('coment'));



        return $show;
    }

    /**
     * Make a form builder.
     * @return Form
     */
    protected function form () {
        $form = new Form(new Assigment());

//      $form->text('status_pay_id', __('status_pay_id'));
//      $form->text('status_assigment_id', __('status_assigment_id'));
        $form->select('customer', __('Customer'))->options(Customer::all()->pluck('name', 'id'));
        $form->select('assistanceVehicle', __('Assistance Vehicle'))->options(AssistanceVehicle::all()->pluck('alias', 'id'));
        $form->select('typeAssigment', __('typeAssigment'))->options(TypeAssigment::all()->pluck('name', 'id'));
        $form->select('vehicle', __('vehicle'))->options(Vehicle::all()->pluck('model', 'id'));

        $form->number('version', __('version'))->min(1800)->max(2100);
        $form->text('color', __('color'))->icon("fa-paint-brush");
        $form->text('plate', __('plate'));

        $form->text('report', __('report'));
        $form->text('sinester', __('sinester'));
        $form->text('location', __('location'));
        $form->text('destination', __('destination'));

        $form->text('cost_lead_change_name', __('cost_lead_change_name'));

        $form->text('distance', __('distance'));
        $form->text('cost_distance', __('cost_distance'));
        $form->text('guard_day', __('guard_day'));
        $form->text('cost_guard', __('cost_guard'));
        $form->text('cost_expired', __('cost_expired'));
        $form->text('extra_load', __('extra_load'));
        $form->text('cost_load', __('cost_load'));
        $form->text('handling', __('handling'));
        $form->text('cost_handling', __('cost_handling'));
        $form->text('armor', __('armor'));
        $form->text('dolly', __('dolly'));

        $form->text('amount', __('amount'));
        $form->text('amountAux', __('amountAux'));
//        $form->text('bill', __('bill'));
//        $form->text('billFilePDF', __('billFilePDF'));
//        $form->text('billFileXml', __('billFileXml'));
        $form->text('tax', __('tax'));
        $form->text('coment', __('coment'));

//        $form->text('type_bulls_id', __('type_bulls_id'));

//        $form->text('date_capture', __('date_capture'));
//        $form->text('date_proses', __('date_proses'));
//        $form->text('date_contact', __('date_contact'));
//        $form->text('date_over', __('date_over'));
//        $form->text('date_pay', __('date_pay'));
//        $form->text('date_lastupdate', __('date_lastupdate'));


        return $form;


    }
}


//    407.80
//   1223.40
//   6117.06
//-----------
//   4893.66
