<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Post\exportToPDF;
use App\Models\Brand;
use App\Models\Vehicle;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Grid\Column;

class VehicleController extends AdminController {

    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\Vehicle-Admin';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {

        $grid = new Grid(new Vehicle());
        $grid->column('id', __('Id'));
        $grid->column('Vehicle B/M')->display(function () {
            return $this->brand['name'].' / '.$this->model;
        });
        $grid->column('typeVehicle.name', __('Type Vehicle'));
        $grid->column('slug', __('Slug'));

//        $grid->column('brand_id', __('Brand'));
//        $grid->column('model', __('Model'))->setAttributes(['style' => 'color:red;']);
//        $grid->column('type_vehicle_id', __('type_vehicle_id'));

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            // the array of data for the current row
            $actions->row;
            $actions->add(new exportToPDF);
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form () {
        $form = new Form(new Vehicle());

        $form->column(1/2,function ($form){
            //        $form->text('model', __('Model'));
            $form->text('slug', __('Slug'));
            //$form->text('brand', __('Brand'));
            $form->select('brand')->options(Brand::all()->pluck('name', 'id'));
        });
        $form->column(1/2,function ($form){
//        $form->text('type_vehicle_id', __('type_vehicle_id'));

            $form->text('model')
                ->creationRules(['required', "unique:vehicle"])
                ->updateRules(['required', "unique:vehicle,name,{{id}}"]);

//        $form->text('brand.name');
//        $form->text('brand.slug');
        });
        return $form;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {

        $show = new Show(Vehicle::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('slug', __('Slug'));
        // $show->field(('Vehicle B/M')->display(function () {
        //     return $this->brand['name'].' / '.$this->model;
        // }));
        $show->typeVehicle('typeVehicle information', function ($typeVehicle) {
            $typeVehicle->setResource('/admin/type_vehicle');
            $typeVehicle->name();
        });
        $show->brand('Brand information', function ($brand) {
            $brand->setResource('/admin/brand');
            $brand->name();
        });

        return $show;
    }

}
