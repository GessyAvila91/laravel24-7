<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use App\Models\Assigment;
use App\Models\Customer;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Database\Eloquent\Model;

class CustomerController extends AdminController {

    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\Customer';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {
        $grid = new Grid(new Customer());

        $grid->column('id', __('id'));
        $grid->column('name', __('name'));
        $grid->column('email', __('email'));
        $grid->column('a_lead', __('a_lead'));
        $grid->column('a_expired', __('a_expired'));
        $grid->column('a_distance', __('a_distance'));
        $grid->column('a_guard', __('a_guard'));
        $grid->column('b_lead', __('b_lead'));
        $grid->column('b_expired', __('b_expired'));
        $grid->column('b_distance', __('b_distance'));
        $grid->column('b_guard', __('b_guard'));
        $grid->column('dolly', __('dolly'));
        $grid->column('cost_load', __('cost_load'));
        $grid->column('armor_a', __('armor_a'));
        $grid->column('armor_b', __('armor_b'));
        $grid->column('basement_cost', __('basement_cost'));
        $grid->column('creation_date', __('creation_date'));
        $grid->column('update_date', __('update_date'));
        $grid->column('active', __('active'));

        return $grid;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {
        $show = new Show(Customer::findOrFail($id));

        $show->field('id', __('id'));
        $show->field('name', __('name'));
        $show->field('email', __('email'));
        $show->field('a_lead', __('a_lead'));
        $show->field('a_expired', __('a_expired'));
        $show->field('a_distance', __('a_distance'));
        $show->field('a_guard', __('a_guard'));
        $show->field('b_lead', __('b_lead'));
        $show->field('b_expired', __('b_expired'));
        $show->field('b_distance', __('b_distance'));
        $show->field('b_guard', __('b_guard'));
        $show->field('dolly', __('dolly'));
        $show->field('cost_load', __('cost_load'));
        $show->field('armor_a', __('armor_a'));
        $show->field('armor_b', __('armor_b'));
        $show->field('basement_cost', __('basement_cost'));
        $show->field('creation_date', __('creation_date'));
        $show->field('update_date', __('update_date'));
        $show->field('active', __('active'));

        return $show;
    }

    /**
     * Make a form builder.
     * @return Form
     */
    protected function form () {
        $form = new Form(new Customer());

        $form->tab('Basic info', function ($form) {
            $form->text('name', __('name'))->required()->icon('fa-user');
            $form->email('email', __('email'))->required();
            $form->divider();
            $form->currency('a_lead', __('a_lead'))->default('1.0')->required()->symbol('$');
            $form->currency('a_expired', __('a_expired'))->default('1.0')->required()->symbol('$');
            $form->currency('a_distance', __('a_distance'))->default('1.0')->required()->symbol('$');
            $form->currency('a_guard', __('a_guard'))->default('1.0')->required()->symbol('$');
            $form->switch('active', __('active'));
        })->tab('ExtraCost', function ($form) {
            $form->column(1/2, function ($form) {
                $form->currency('b_lead', __('b_lead'))->default('1.0')->symbol('$');
                $form->currency('b_expired', __('b_expired'))->default('1.0')->symbol('$');
                $form->currency('b_distance', __('b_distance'))->default('1.0')->symbol('$');
                $form->currency('b_guard', __('b_guard'))->default('1.0')->symbol('$');
            });
        })->tab('Jobs', function ($form) {
            $form->currency('dolly', __('dolly'))->default('1.0')->required()->symbol('$');
            $form->currency('cost_load', __('cost_load'))->default('1.0')->required()->symbol('$');
            $form->currency('armor_a', __('armor_a'))->default('1.0')->required()->symbol('$');
            $form->currency('armor_b', __('armor_b'))->default('1.0')->required()->symbol('$');
            $form->currency('basement_cost', __('basement_cost'))->default('1.0')->required()->symbol('$');
        });
//        $form->datetime('creation_date', __('creation_date'));
//        $form->text('update_date', __('update_date'));

        return $form;
    }
}
