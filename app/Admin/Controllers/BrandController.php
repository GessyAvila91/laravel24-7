<?php

namespace App\Admin\Controllers;

use App\Models\Brand;
use App\Models\BrandExtension;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Extensions\Popover;
use Encore\Admin\Grid\Displayers\AbstractDisplayer;


class BrandController extends AdminController {

    /**
     * Title for current resource.
     * @var string
     */
    protected $title = 'App\Brand';

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid () {

        $grid = new Grid(new Brand());
        $grid->id()->sortable();
        $grid->name()->sortable();
        $grid->slug();

        $grid->vehicle('Vehicle count')->display(function ($vehicle) {
            $BrandExtension = new BrandExtension();
            $echo = "";
            $par = "";

            $count = count($vehicle);

            if ($count < 10){
                $echo = $echo.$BrandExtension->listVehicleModal($count,$this->id,$this->name,$vehicle);
            }if ($count >= 10 and $count <= 20){
                $echo = $echo.$BrandExtension->listVehicleModal($count,$this->id,$this->name,$vehicle);
            }if ($count > 20){
                $echo = $echo.$BrandExtension->listVehicleModal($count,$this->id,$this->name,$vehicle);
            }
            return $echo;
        });

        return $grid;
    }


    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail ($id) {
        $show = new Show(Brand::findOrFail($id));
        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('slug', __('Slug'));

        return $show;
    }

    /**
     * Make a form builder.
     * @return Form
     */
    protected function form () {
        $form = new Form(new Brand());
        $form->text('name', __('Name'));
        $form->text('slug', __('Slug'));
        return $form;
    }
}
