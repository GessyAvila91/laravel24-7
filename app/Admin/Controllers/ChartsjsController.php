<?php

namespace App\Admin\Controllers;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Box;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChartsjsController extends Controller {

    public function index (Content $content) {
        $content->header('chart');
        $content->description('.....');

        return $content->body(view('admin.chartsjs.chartsjs'));
    }
}
