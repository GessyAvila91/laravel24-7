<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'vehicle';

    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function typeVehicle(){
        return $this->belongsTo(TypeVehicle::class);
    }

    public function assigment(){
        return $this->belongsTo(Assigment::class);
    }

}
