<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assigment extends Model {
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'assigment';

    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function vehicle () {
        return $this->belongsTo(Vehicle::class);
    }

    public function assistanceVehicle () {
        return $this->belongsTo(AssistanceVehicle::class);
    }

    public function customer (){
        return $this->belongsTo(Customer::class);
    }

    public function typeAssigment (){
        return $this->belongsTo(TypeAssigment::class);
    }



}
