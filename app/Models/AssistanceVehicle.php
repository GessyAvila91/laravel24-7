<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssistanceVehicle extends Model {
    //

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'assistance_vehicle';

    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;

    public function assigment(){
        return $this->belongsTo(Assigment::class);
    }


}
