<?php

namespace App\Models;

class BrandExtension  {

    /**
     * BrandExtension constructor.
     */
    public function __construct () {
    }

    /**
     * Make a Modal for Brand Controller listing all Vehicles from Brands on a list
     *
     * @param $count
     * @param $id
     * @param $name
     * @param $par
     * @return string
     */
    public function listVehicleModal($count,$id,$name,$vehicle){

        if ($count == 0){
            return "
            <div class='center-block'>
                <span class='badge badge-pill badge-secondary'>{$count}</span>
            </div>
            ";
        }
        $par = "";
        foreach ($vehicle as $valor) {
            $par = $par."
            <li class='list-group-item d-flex justify-content-between align-items-center'>"
            .$valor["model"]."
            <span>
             <a href=\"vehicle/".$valor["id"]."\" class=\"btn btn-xs btn-info pull-right\" class='close' data-dismiss='modal'>Show</a>
            </span>
            </li>";

            ///http://127.0.0.1:8000/admin/vehicle/{id}
            ///<a href="{{ url('/problems/' . $problem->id . '/edit') }}" class="btn btn-xs btn-info pull-right">Edit</a>
        }


        return "
                <button id='#notification-button' type='button' class='btn btn-info' data-toggle='modal'data-target='#myModal".$id."'>
                List
                <span id='notifications-badge' class='badge badge-secondary'>{$count}</span>
                </button>
                <!-- Modal -->
                <div id='myModal".$id."' class='modal fade' role='dialog'>
                  <div class='modal-dialog'>
                    <!-- Modal content-->
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal'>&times;</button>
                        <h4 class='modal-title'>".$name."</h4>
                      </div>
                      <div class='modal-body'>
                        <ul class='list-group'>".$par."</ul>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                ";
    }

}
